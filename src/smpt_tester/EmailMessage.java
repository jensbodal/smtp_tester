package smpt_tester;

/**
 * <h1>EmailMessage.java</h1>
 *
 *
 * @author Jens Bodal

 */
public class EmailMessage {

    private String toAddress;
    private String fromAddress;
    private String subject;
    private String body;

    /**
     * Represents the components of a basic email message
     * @param toAddress the smtp address to send the mail to
     * @param fromAddress the smtp address to show in the from field
     * @param subject the subject of the email
     * @param body the message body of the email
     */
    public EmailMessage(String toAddress, String fromAddress, String subject, 
            String body) {
        setToAddress(toAddress);
        setFromAddress(fromAddress);
        setSubject(subject);
        setBody(body);
    }

    private boolean isValidEmailAddress(String emailAddress) {
        return emailAddress.contains("@");
    }

    /**
     * Returns true if the email message is complete
     * @return true or false depending on if all the email fields are complete
     */
    public boolean isComplete() {
        return toAddress != null
                && fromAddress != null
                && subject != null
                && body != null;
    }

    /**
     * Getter to retrieve the to address of the email message
     * @return the smtp address in the To: field
     */
    public String getToAddress() {
        return this.toAddress;
    }

    private void setToAddress(String toAddress) {
        if (isValidEmailAddress(toAddress)) {
            this.toAddress = toAddress;
        }
    }

    /**
     * Getter to retrieve the from address of the email message
     * @return the smtp address in the From: field
     */
    public String getFromAddress() {
        return this.fromAddress;
    }

    private void setFromAddress(String fromAddress) {
        if (isValidEmailAddress(fromAddress)) {
            this.fromAddress = fromAddress;
        }
    }

    /**
     * Getter to retrieve the subject line of the email message
     * @return the text in the subject line of the email
     */
    public String getSubject() {
        return this.subject;
    }

    private void setSubject(String subject) {
        if (subject.length() > 0) {
            this.subject = subject;
        }
    }

    /**
     * Getter to retrieve the content of the email message body
     * @return the text in the message body of the email
     */
    public String getBody() {
        return this.body;
    }

    private void setBody(String body) {
        if (body.length() > 0) {
            this.body = body;
        }
    }

}
