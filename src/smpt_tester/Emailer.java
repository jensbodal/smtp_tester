package smpt_tester;

import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * <h1>Emailer.java</h1>
 * Creates an object which represents the SMTP server to send an email from
 *
 * @author Jens Bodal
 * @version 1.0
 *
 */
public class Emailer {

    private final String[] credentials;
    private String smtpHost;
    private String port;
    private boolean useTLS;
    private boolean useSSL;
    private boolean authenticationEnabled;

    /**
     * Creates an Emailer object, there are no arguments to pass
     */
    public Emailer() {
        authenticationEnabled = true;
        credentials = new String[2];
    }

    /**
     * Stores credentials in a simple array
     * @param username user email address 
     * @param password user email password
     */
    public void setCredentials(String username, String password) {
        this.credentials[0] = username;
        this.credentials[1] = password;
    }

    private String getUsername() {
        return credentials[0];
    }

    private String getPassword() {
        return credentials[1];
    }

    /**
     * Disabling authentication will only work if your ISP allows outgoing
     * packets on port 25; Outgoing packets on port 25 is generally not
     * available for non-business class ISP customers
     *
     * @param enabled specifies whether email will be sent using authenticated
     * credentials
     */
    public void enableAuthentication(boolean enabled) {
        this.authenticationEnabled = enabled;
    }

    /**
     * Specifies whether to send email using the TLS protocol
     *
     * @param enabled specifies whether to use TLS or not
     */
    public void useTLS(boolean enabled) {
        this.useTLS = enabled;
    }

    /**
     * Specifies whether to send email using the SSL protocol
     *
     * @param enabled specifies whether to use SSL or not
     */
    public void useSSL(boolean enabled) {
        this.useSSL = enabled;
    }

    /**
     * SMTP Host server to use, either a domain or an IP address
     *
     * @param host the SMTP Host server, either a domain or an IP address
     */
    public void setSmtpHost(String host) {
        this.smtpHost = host;
    }

    /**
     * SMTP Port to use, defaults are generally 25, 465, or 587
     *
     * @param port SMTP port to use
     */
    public void setPort(int port) {
        this.port = String.valueOf(port);
    }

    private Authenticator enableAuthentication() {
        return new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(getUsername(), getPassword());
            }
        };
    }

    private Authenticator disableAuthentication() {
        return new Authenticator() {
        };
    }

    /**
     * Primary method for class which will send an email based on the given
     * EmailMessage message and provided SMTP settings
     * @param message the EmailMessage message to send
     * @throws AddressException bad email address provided in to or from field
     * @throws MessagingException issue with the format of the email message
     */
    public void sendEmail(EmailMessage message)
            throws AddressException, MessagingException {
        Properties props = new Properties();

        props.put("mail.smtp.host", this.smtpHost);
        props.put("mail.smtp.port", this.port);
        props.put("mail.smtp.auth", this.authenticationEnabled);
        props.put("mail.smtp.starttls.enable", this.useTLS);
        props.put("mail.smtp.ssl.enable", this.useSSL);
        props.put("mail.smtp.timeout", 15000);
        Authenticator authenticator;
        if (authenticationEnabled) {
            authenticator = enableAuthentication();
        }
        else {
            authenticator = disableAuthentication();
        }

        Session session = Session.getInstance(props, authenticator);
        Message ourEmail = new MimeMessage(session);
        InternetAddress toAddress
                = new InternetAddress(message.getToAddress());
        InternetAddress fromAddress
                = new InternetAddress(message.getFromAddress());

        ourEmail.setFrom(fromAddress);
        ourEmail.setRecipient(Message.RecipientType.TO, toAddress);
        ourEmail.setSubject(message.getSubject());
        ourEmail.setText(message.getBody());
        Transport.send(ourEmail);
    }

}
