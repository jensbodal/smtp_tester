package smpt_tester;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.swing.JFrame;

/**
 * <h1>EmailerController.java</h1>
 *
 */
public class EmailerController {
    private final Emailer emailer;
    private final MainFormView view;
    
    private STATUS status;

    /**
     * Initializes controller with the Emailer and MainFormView
     *
     * @param emailer the Emailer object to use
     * @param view the Swing view to use
     */
    public EmailerController(Emailer emailer, MainFormView view) {
        this.emailer = emailer;
        this.view = view;
        view.sendButton.addActionListener(sendListener());
    }

    /**
     * Called after initialization to show the GUI
     */
    public void updateView() {
        view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        view.setVisible(true);
    }

    /**
     * Action listener which parses all of the fields from the GUI and sends an
     * email based on the criteria
     *
     * @return this ActionListener
     */
    private ActionListener sendListener() {
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    String toAddress = view.toArea.getText();
                    String fromAddress = view.fromArea.getText();
                    String subject = view.subjectArea.getText();
                    String body = view.messageArea.getText();
                    EmailMessage message = new EmailMessage(
                            toAddress, fromAddress, subject, body
                    );
                    emailer.setCredentials(
                            view.usernameArea.getText(), view.passwordArea.getText());
                    emailer.setSmtpHost(view.smtpHostArea.getText());
                    emailer.setPort(Integer.parseInt(view.smtpPortarea.getText()));
                    emailer.enableAuthentication(view.authArea.isChecked());
                    emailer.useSSL(view.sslArea.isChecked());
                    emailer.useTLS(view.tlsArea.isChecked());
                    if (message.isComplete()) {
                        emailer.sendEmail(message);
                        status = STATUS.SUCCESS;
                    }
                    else {
                        status = STATUS.BAD_EMAIL;
                    }
                }
                catch (AddressException ex) {
                    status = STATUS.BAD_ADDRESS;
                    view.appendDebugText(ex.toString());
                }
                catch (MessagingException ex) {
                    status = STATUS.BAD_HOST;
                    view.appendDebugText(ex.toString());
                }
                catch (NumberFormatException ex) {
                    status = STATUS.INVALID_PORT;
                    view.appendDebugText(ex.toString());
                }
                catch (NullPointerException npe) {
                    status = STATUS.UNKNOWN;
                    view.appendDebugText(npe.toString());
                }
                catch (Exception ex) {
                    status = STATUS.UNKNOWN;
                    view.appendDebugText(ex.toString());
                }
                view.appendDebugText(status.message());
            }
        };

    }

    private enum STATUS {
        BAD_ADDRESS("Invalid email address in to or from field"),
        BAD_EMAIL("Missing one or more required fields in the email message"),
        BAD_HOST("Please check the SMTP host address or port"),
        INVALID_PORT("Please check if port number is valid"),
        SUCCESS("Email has been transmitted successfully"),
        UNKNOWN("An unknown error has occurred");
        
        private final String message;
        
        STATUS(String message) {
            this.message = message;
        }
        
        private String message() {
            return message;
        }
    }
}
