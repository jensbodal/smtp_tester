package smpt_tester;

/**
 * <h1>Launcher.java</h1>
 * Program entry point, launches swing GUI
 *
 * @author Jens Bodal
 */ 
public class Launcher {

    /**
     * Program entry point
     * @param args the command line arguments (none accepted)
     */
    public static void main(String[] args) {
        MainFormView view = new MainFormView();
        Emailer emailer = new Emailer();
        EmailerController controller = new EmailerController(emailer, view);
        controller.updateView();
    }
}