package smpt_tester;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

/**
 * <h1>MainFormView.java</h1>
 * Swing view for the Emailer object. Contains custom innerclass called
 * TitleFieldCombo which contains a component and its associated title for ease
 * of use in displaying the various components.
 */
public class MainFormView extends JFrame {

    private final int FRAME_WIDTH = 800;
    private final int FRAME_HEIGHT = 300;
    private final int FRAME_X = 500;
    private final int FRAME_Y = 200;
    private final Dimension textFieldDimensions = new Dimension(200, 25);
    private final Dimension portFieldDimensions = new Dimension(20, 25);
    private final Dimension checkBoxDimensions = new Dimension(20, 20);
    private final String checkBoxSpacer = "     ";

    /**
     * Stores the title and component for the SMTP email address
     */
    public TitleFieldCombo usernameArea;

    /**
     * Stores the title and component for the SMTP password
     */
    public TitleFieldCombo passwordArea;

    /**
     * Stores the title and component for the SMTP host
     */
    public TitleFieldCombo smtpHostArea;

    /**
     * Stores the title and component for the SMTP port
     */
    public TitleFieldCombo smtpPortarea;

    /**
     * Stores the title and component for whether to use TLS
     */
    public TitleFieldCombo tlsArea;

    /**
     * Stores the title and component for whether to use SSL
     */
    public TitleFieldCombo sslArea;

    /**
     * Stores the title and component for whether to use authentication
     */
    public TitleFieldCombo authArea;

    /**
     * Stores the title and component for the email to address
     */
    public TitleFieldCombo toArea;

    /**
     * Stores the title and component for the email from address
     */
    public TitleFieldCombo fromArea;

    /**
     * Stores the title and component for the email subject
     */
    public TitleFieldCombo subjectArea;

    /**
     * Stores the title and component for the email body
     */
    public TitleFieldCombo messageArea;

    /**
     * The button which runs the program to send the email
     */
    public JButton sendButton;

    /**
     * The JTextArea which is meant to contain debugging information
     */
    public JTextArea debugArea;

    /**
     * Creates the various areas of the inner JPanel and packs them
     */
    public MainFormView() {
        this.setBounds(FRAME_X, FRAME_Y, FRAME_WIDTH, FRAME_HEIGHT);
        createAreas();
        this.add(mainPanel());
        this.pack();
        this.setResizable(false);
    }

    private JPanel mainPanel() {
        JPanel panel = new JPanel();
        JPanel smtpPanel = new JPanel();
        JPanel messagePanel = new JPanel();
        panel.setLayout(new BorderLayout());
        smtpPanel.setLayout(new BorderLayout());
        messagePanel.setLayout(new BorderLayout());
        smtpPanel.add(smtpTitles(), BorderLayout.WEST);
        smtpPanel.add(smtpComponents(), BorderLayout.CENTER);
        smtpPanel.add(checkboxArea(), BorderLayout.SOUTH);
        messagePanel.add(messageTitles(), BorderLayout.WEST);
        messagePanel.add(messageComponents(), BorderLayout.CENTER);
        messagePanel.add(sendButton(), BorderLayout.SOUTH);
        panel.add(smtpPanel, BorderLayout.NORTH);
        panel.add(messagePanel, BorderLayout.CENTER);
        panel.add(debugPanel(), BorderLayout.SOUTH);
        return panel;
    }

    private void createAreas() {
        usernameArea = new TitleFieldCombo(
                "Username: ",
                JTextField.class,
                textFieldDimensions
        );

        passwordArea = new TitleFieldCombo(
                "Password: ",
                JPasswordField.class,
                textFieldDimensions
        );

        smtpHostArea = new TitleFieldCombo(
                "SMTP Host: ",
                JTextField.class,
                textFieldDimensions
        );

        smtpPortarea = new TitleFieldCombo(
                "Port: ",
                JTextField.class,
                portFieldDimensions
        );

        authArea = new TitleFieldCombo(
                "Use Authentication: ",
                JCheckBox.class,
                checkBoxDimensions
        );

        tlsArea = new TitleFieldCombo(
                "Use TLS: ",
                JCheckBox.class,
                checkBoxDimensions
        );
        sslArea = new TitleFieldCombo(
                "Use SSL: ",
                JCheckBox.class,
                checkBoxDimensions
        );

        toArea = new TitleFieldCombo(
                "To Address: ",
                JTextField.class,
                textFieldDimensions
        );

        fromArea = new TitleFieldCombo(
                "From Address: ",
                JTextField.class,
                textFieldDimensions
        );

        subjectArea = new TitleFieldCombo(
                "Subject: ",
                JTextField.class,
                textFieldDimensions
        );

        messageArea = new TitleFieldCombo(
                "Message: ",
                JTextArea.class,
                textFieldDimensions
        );

    }

    private JPanel smtpTitles() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(4, 1));
        panel.add(usernameArea.title);
        panel.add(passwordArea.title);
        panel.add(smtpHostArea.title);
        panel.add(smtpPortarea.title);
        return panel;
    }

    private JPanel smtpComponents() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(4, 1));
        panel.add(usernameArea.component);
        panel.add(passwordArea.component);
        panel.add(smtpHostArea.component);
        panel.add(smtpPortarea.component);
        return panel;
    }

    private JPanel checkboxArea() {
        JPanel panel = new JPanel();
        JPanel subPanel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.LEFT));
        panel.add(authArea.title);
        panel.add(authArea.component);
        panel.add(new JLabel(checkBoxSpacer));
        panel.add(tlsArea.title);
        panel.add(tlsArea.component);
        panel.add(new JLabel(checkBoxSpacer));
        panel.add(sslArea.title);
        panel.add(sslArea.component);
        return panel;
    }

    private JPanel messageTitles() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(4, 1));
        panel.add(toArea.title);
        panel.add(fromArea.title);
        panel.add(subjectArea.title);
        panel.add(messageArea.title);
        return panel;
    }

    private JPanel messageComponents() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(4, 1));
        panel.add(toArea.component);
        panel.add(fromArea.component);
        panel.add(subjectArea.component);
        panel.add(messageArea.component);
        return panel;
    }

    private JButton sendButton() {
        sendButton = new JButton();
        sendButton.setText("Send Test Email");
        return sendButton;
    }

    /**
     * Appends text to the JTextArea debugArea component
     *
     * @param str the text to append
     */
    public void appendDebugText(String str) {
        debugArea.append(str + "\n");
    }

    private JPanel debugPanel() {
        JPanel mainPanel = new JPanel();
        JPanel panelLeft = new JPanel();
        JPanel panelRight = new JPanel();

        mainPanel.setLayout(new BorderLayout());
        panelLeft.setLayout(new BoxLayout(panelLeft, BoxLayout.Y_AXIS));
        panelRight.setLayout(new BoxLayout(panelRight, BoxLayout.Y_AXIS));

        debugArea = new JTextArea();
        JTextArea notesArea = new JTextArea();

        JScrollPane leftPane = new JScrollPane(debugArea);
        JScrollPane rightPane = new JScrollPane(notesArea);

        debugArea.setLineWrap(true);
        debugArea.setWrapStyleWord(true);
        notesArea.setLineWrap(true);
        notesArea.setWrapStyleWord(true);
        
        debugArea.setEditable(false);

        leftPane.setVerticalScrollBar(new JScrollBar());
        rightPane.setVerticalScrollBar(new JScrollBar());
        
        Dimension mainDimension = new Dimension(FRAME_WIDTH, FRAME_HEIGHT);
        Dimension lrDimension = new Dimension(FRAME_WIDTH / 2, FRAME_HEIGHT);
        panelLeft.setPreferredSize(lrDimension);
        panelRight.setPreferredSize(lrDimension);
        mainPanel.setPreferredSize(mainDimension);

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                leftPane, rightPane);
        
        mainPanel.add(splitPane);
        splitPane.setResizeWeight(.5);
        return mainPanel;
    }

    /**
     * Custom inner class to associate a JComponent with its appropriate title
     */
    public class TitleFieldCombo {

        private JLabel title = new JLabel();
        private JComponent component;

        TitleFieldCombo(String title, Class fieldType, Dimension dimension) {
            try {
                this.title.setText(title);
                component = (JComponent) fieldType.newInstance();
                component.setPreferredSize(dimension);

            }
            catch (Exception ex) {
                debugArea.append(ex.toString());
            }
        }

        /**
         * Retrieves the text of a JTextComponent; if the component is not a
         * JTextComponent then it will throw an IllegalArgumentException
         *
         * @return the text of the JTextComponent
         * @throws IllegalArgumentException if component is not a JTextComponent
         */
        public String getText() throws IllegalArgumentException {
            if (JTextComponent.class.isInstance(component)) {
                JTextComponent forceTextComponent = (JTextComponent) component;
                return forceTextComponent.getText();
            }
            else {
                throw new IllegalArgumentException(
                        "Component must be a JTextComponent");
            }
        }

        /**
         * Determines if given JCheckBox is checked; if the given component is
         * not a JCheckBox then an IllegalArgument is thrown
         *
         * @return true if JCheckBox is checked, returns false otherwise
         * @throws IllegalArgumentException if component is not a JCheckBox
         */
        public boolean isChecked() throws IllegalArgumentException {
            if (JCheckBox.class.isInstance(component)) {
                JCheckBox checkbox = (JCheckBox) component;
                return checkbox.isSelected();
            }
            else {
                throw new IllegalArgumentException(
                        "Component must be a JCheckBox");
            }
        }
    }
}
